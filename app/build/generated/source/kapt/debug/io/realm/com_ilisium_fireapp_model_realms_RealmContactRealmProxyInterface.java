package io.realm;


public interface com_ilisium_fireapp_model_realms_RealmContactRealmProxyInterface {
    public String realmGet$name();
    public void realmSet$name(String value);
    public RealmList<com.ilisium.fireapp.model.realms.PhoneNumber> realmGet$realmList();
    public void realmSet$realmList(RealmList<com.ilisium.fireapp.model.realms.PhoneNumber> value);
    public String realmGet$jsonString();
    public void realmSet$jsonString(String value);
}
