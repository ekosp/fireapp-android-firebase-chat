package io.realm;


public interface com_ilisium_fireapp_model_realms_DeletedMessageRealmProxyInterface {
    public String realmGet$messageId();
    public void realmSet$messageId(String value);
}
