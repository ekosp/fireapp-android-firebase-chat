package io.realm;


public interface com_ilisium_fireapp_model_realms_BroadcastRealmProxyInterface {
    public String realmGet$broadcastId();
    public void realmSet$broadcastId(String value);
    public String realmGet$createdByNumber();
    public void realmSet$createdByNumber(String value);
    public long realmGet$timestamp();
    public void realmSet$timestamp(long value);
    public RealmList<com.ilisium.fireapp.model.realms.User> realmGet$users();
    public void realmSet$users(RealmList<com.ilisium.fireapp.model.realms.User> value);
}
