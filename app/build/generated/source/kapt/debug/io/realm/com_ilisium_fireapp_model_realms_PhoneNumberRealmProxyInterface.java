package io.realm;


public interface com_ilisium_fireapp_model_realms_PhoneNumberRealmProxyInterface {
    public String realmGet$number();
    public void realmSet$number(String value);
}
