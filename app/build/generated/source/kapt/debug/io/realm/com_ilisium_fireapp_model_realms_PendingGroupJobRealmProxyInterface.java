package io.realm;


public interface com_ilisium_fireapp_model_realms_PendingGroupJobRealmProxyInterface {
    public String realmGet$groupId();
    public void realmSet$groupId(String value);
    public int realmGet$type();
    public void realmSet$type(int value);
    public com.ilisium.fireapp.model.realms.GroupEvent realmGet$groupEvent();
    public void realmSet$groupEvent(com.ilisium.fireapp.model.realms.GroupEvent value);
}
