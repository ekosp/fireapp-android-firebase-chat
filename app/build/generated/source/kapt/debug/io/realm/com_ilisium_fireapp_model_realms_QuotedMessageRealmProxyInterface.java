package io.realm;


public interface com_ilisium_fireapp_model_realms_QuotedMessageRealmProxyInterface {
    public String realmGet$messageId();
    public void realmSet$messageId(String value);
    public String realmGet$fromId();
    public void realmSet$fromId(String value);
    public String realmGet$fromPhone();
    public void realmSet$fromPhone(String value);
    public String realmGet$toId();
    public void realmSet$toId(String value);
    public int realmGet$type();
    public void realmSet$type(int value);
    public String realmGet$content();
    public void realmSet$content(String value);
    public String realmGet$metadata();
    public void realmSet$metadata(String value);
    public String realmGet$mediaDuration();
    public void realmSet$mediaDuration(String value);
    public String realmGet$thumb();
    public void realmSet$thumb(String value);
    public String realmGet$fileSize();
    public void realmSet$fileSize(String value);
    public com.ilisium.fireapp.model.realms.RealmContact realmGet$contact();
    public void realmSet$contact(com.ilisium.fireapp.model.realms.RealmContact value);
    public com.ilisium.fireapp.model.realms.RealmLocation realmGet$location();
    public void realmSet$location(com.ilisium.fireapp.model.realms.RealmLocation value);
    public boolean realmGet$isBroadcast();
    public void realmSet$isBroadcast(boolean value);
    public String realmGet$encryptionType();
    public void realmSet$encryptionType(String value);
    public com.ilisium.fireapp.model.realms.Status realmGet$status();
    public void realmSet$status(com.ilisium.fireapp.model.realms.Status value);
}
