package io.realm;


public interface com_ilisium_fireapp_model_realms_StatusSeenByRealmProxyInterface {
    public com.ilisium.fireapp.model.realms.User realmGet$user();
    public void realmSet$user(com.ilisium.fireapp.model.realms.User value);
    public long realmGet$seenAt();
    public void realmSet$seenAt(long value);
}
