/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.ilisium.fireapp;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.ilisium.fireapp";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 32;
  public static final String VERSION_NAME = "2.1.3";
}
