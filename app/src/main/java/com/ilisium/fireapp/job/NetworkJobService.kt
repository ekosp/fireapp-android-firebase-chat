package com.ilisium.fireapp.job

import android.app.job.JobInfo
import android.app.job.JobParameters
import android.content.ComponentName
import android.content.Context
import android.os.Build
import android.os.PersistableBundle
import androidx.annotation.RequiresApi
import com.ilisium.fireapp.model.realms.GroupEvent
import com.ilisium.fireapp.model.realms.JobId
import com.ilisium.fireapp.model.realms.User
import com.ilisium.fireapp.utils.DownloadManager
import com.ilisium.fireapp.utils.IntentUtils
import com.ilisium.fireapp.utils.JobSchedulerSingleton
import com.ilisium.fireapp.utils.RealmHelper
import com.ilisium.fireapp.utils.network.CallsManager
import com.ilisium.fireapp.utils.network.GroupManager
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.Main

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
class NetworkJobService :   com.ilisium.fireapp.job.BaseJob() {
    var groupManager = GroupManager()
    var callsManager = CallsManager()
    var downloadManager = DownloadManager()


    private val parentJob = SupervisorJob()
    private val scope = CoroutineScope(Main + parentJob)


    override fun onStartJob(jobParameters: JobParameters): Boolean {


        val extras = jobParameters.extras
        val action = extras.getString(  com.ilisium.fireapp.utils.IntentUtils.ACTION_TYPE)
        val isVoiceMessage = isVoiceMessage(jobParameters)
        if (action ==   com.ilisium.fireapp.utils.IntentUtils.INTENT_ACTION_UPDATE_GROUP) {
            val groupEventBundle = extras.getPersistableBundle(  com.ilisium.fireapp.utils.IntentUtils.EXTRA_GROUP_EVENT)
            val contextStart = groupEventBundle!!.getString(  com.ilisium.fireapp.utils.IntentUtils.EXTRA_CONTEXT_START)
            val eventType = groupEventBundle.getInt(  com.ilisium.fireapp.utils.IntentUtils.EXTRA_EVENT_TYPE)
            val contextEnd = groupEventBundle.getString(  com.ilisium.fireapp.utils.IntentUtils.EXTRA_CONTEXT_END)
            val groupEvent =   com.ilisium.fireapp.model.realms.GroupEvent(
                contextStart,
                eventType,
                contextEnd
            )
            val groupId = extras.getString(  com.ilisium.fireapp.utils.IntentUtils.EXTRA_GROUP_ID)

            groupManager.updateGroup(groupId!!, groupEvent)
                .subscribe({ user: List<  com.ilisium.fireapp.model.realms.User?>? ->
                      com.ilisium.fireapp.utils.RealmHelper.getInstance().deletePendingGroupCreationJob(groupId)
                    onFinishJob(jobParameters, false)
                })

                { throwable: Throwable? ->
                    onFinishJob(jobParameters, true)
                }.addTo(disposables)

        } else if (action ==   com.ilisium.fireapp.utils.IntentUtils.INTENT_ACTION_FETCH_AND_CREATE_GROUP) {
            val groupId = extras.getString(  com.ilisium.fireapp.utils.IntentUtils.EXTRA_GROUP_ID)
            groupManager.fetchAndCreateGroup(groupId!!)
                .subscribe({ user:   com.ilisium.fireapp.model.realms.User? ->
                    if (groupId != null) {
                          com.ilisium.fireapp.utils.RealmHelper.getInstance().deletePendingGroupCreationJob(groupId)
                    }
                    onFinishJob(jobParameters, groupId == null)
                })

                { throwable: Throwable? ->
                    onFinishJob(jobParameters, groupId == null)
                }.addTo(disposables)

        } else if (action ==   com.ilisium.fireapp.utils.IntentUtils.INTENT_ACTION_FETCH_USER_GROUPS_AND_BROADCASTS) {
            //we are keeping this since some users may still have this on older versions of the app
            onFinishJob(jobParameters, false)
        } else if (action ==   com.ilisium.fireapp.utils.IntentUtils.INTENT_ACTION_SET_CALL_ENDED) {
            val callId = extras.getString(  com.ilisium.fireapp.utils.IntentUtils.CALL_ID)
            val otherUid = extras.getString(  com.ilisium.fireapp.utils.IntentUtils.OTHER_UID)
            val isIncoming = extras.getBoolean(  com.ilisium.fireapp.utils.IntentUtils.IS_INCOMING, true)

            callsManager.setCallEnded(callId!!, otherUid!!, isIncoming)
                .subscribe({ onFinishJob(jobParameters, false) })
                { throwable: Throwable? -> onFinishJob(jobParameters, true) }.addTo(disposables)

        } else if (action ==   com.ilisium.fireapp.utils.IntentUtils.INTENT_ACTION_SET_CALL_DECLINED_FOR_GROUP) {
            val callId = extras.getString(  com.ilisium.fireapp.utils.IntentUtils.CALL_ID)
            val groupId = extras.getString(  com.ilisium.fireapp.utils.IntentUtils.EXTRA_GROUP_ID)

            callsManager.setCallRejectedForGroup(callId!!, groupId!!)
                .subscribe({ onFinishJob(jobParameters, false) })
                { throwable: Throwable? -> onFinishJob(jobParameters, true) }
                .addTo(disposables)

        } else {
            val messageId = extras.getString(  com.ilisium.fireapp.utils.IntentUtils.EXTRA_MESSAGE_ID)
            val chatId = extras.getString(  com.ilisium.fireapp.utils.IntentUtils.EXTRA_CHAT_ID)
            if (action ==   com.ilisium.fireapp.utils.IntentUtils.INTENT_ACTION_UPDATE_MESSAGE_STATE) {
                val myUid = extras.getString(  com.ilisium.fireapp.utils.IntentUtils.EXTRA_MY_UID)
                val state = extras.getInt(  com.ilisium.fireapp.utils.IntentUtils.EXTRA_STAT, 0)
                fireManager.updateMessagesState(myUid!!, messageId!!, state, isVoiceMessage)
                    .subscribe({ jobFinished(jobParameters, false) })
                    { throwable: Throwable? ->
                        jobFinished(jobParameters, true)
                    }.addTo(disposables)
            } else if (action ==   com.ilisium.fireapp.utils.IntentUtils.INTENT_ACTION_UPDATE_VOICE_MESSAGE_STATE) {
                val myUid = extras.getString(  com.ilisium.fireapp.utils.IntentUtils.EXTRA_MY_UID)
                fireManager.updateVoiceMessageStat(myUid!!, messageId!!)
                    .subscribe({ jobFinished(jobParameters, false) })
                    { throwable: Throwable? -> jobFinished(jobParameters, true) }
                    .addTo(disposables)
            } else if (action ==   com.ilisium.fireapp.utils.IntentUtils.INTENT_ACTION_HANDLE_REPLY) {
                val message =   com.ilisium.fireapp.utils.RealmHelper.getInstance().getMessage(messageId, chatId)
                scope.launch {
                        if (message != null) {
                            downloadManager.sendMessage(
                                message,
                                scope,
                                object : DownloadManager.OnComplete {
                                    override fun onComplete(isSuccessful: Boolean) {
                                        if (isSuccessful) {
                                            //set other unread messages as read
                                            if (!message.isGroup) fireManager.setMessagesAsRead(
                                                this@NetworkJobService,
                                                message.chatId
                                            )
                                        }
                                        onFinishJob(jobParameters, !isSuccessful)
                                    }
                                })

                    }
                }

            } else {
                scope.launch {
                        val message =   com.ilisium.fireapp.utils.RealmHelper.getInstance().getMessage(messageId, chatId)
                        if (message != null) {
                            downloadManager.request(
                                message,
                                scope,
                                object : DownloadManager.OnComplete {
                                    override fun onComplete(isSuccess: Boolean) {
                                        onFinishJob(jobParameters, !isSuccess)
                                    }
                                })
                        }

                }

            }
        }
        return true
    }

    private fun isVoiceMessage(jobParameters: JobParameters): Boolean {
        return jobParameters.extras.getString(  com.ilisium.fireapp.utils.IntentUtils.ACTION_TYPE) ==   com.ilisium.fireapp.utils.IntentUtils.INTENT_ACTION_UPDATE_VOICE_MESSAGE_STATE
    }

    override fun onStopJob(jobParameters: JobParameters): Boolean {
        val jobId = jobParameters.jobId
        val isVoiceMessage = isVoiceMessage(jobParameters)
        val id =   com.ilisium.fireapp.utils.RealmHelper.getInstance().getJobId(jobId, isVoiceMessage)
        cancelCoroutineJob()
        disposables.dispose()
        return true
    }

    private fun cancelCoroutineJob() = try {
        parentJob.cancel()
    } catch (e: Exception) {
    }

    private fun onFinishJob(jobParameters: JobParameters, needsReschedule: Boolean) {
        if (!needsReschedule) {
            val id = jobParameters.extras.getString(  com.ilisium.fireapp.utils.IntentUtils.ID)
              com.ilisium.fireapp.utils.RealmHelper.getInstance().deleteJobId(id, isVoiceMessage(jobParameters))
        }
        jobFinished(jobParameters, needsReschedule)
    }

    companion object {
        @JvmStatic
        fun schedule(context: Context?, id: String?, bundle: PersistableBundle) {
            val component = ComponentName(context!!, NetworkJobService::class.java)
            val action = bundle.getString(  com.ilisium.fireapp.utils.IntentUtils.ACTION_TYPE)

            //if it's a voice message then we want to generate a new id
            // because in case if  the action was 'update message state' both will have the same id
            val jobId =   com.ilisium.fireapp.model.realms.JobId(
                id,
                action ==   com.ilisium.fireapp.utils.IntentUtils.INTENT_ACTION_UPDATE_VOICE_MESSAGE_STATE
            )
              com.ilisium.fireapp.utils.RealmHelper.getInstance().saveJobId(jobId)
            val mJobId = jobId.jobId
            val builder = JobInfo.Builder(mJobId, component)
                .setMinimumLatency(1)
                .setOverrideDeadline(1)
                .setPersisted(true)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setExtras(bundle)
            val allPendingJobs =   com.ilisium.fireapp.utils.JobSchedulerSingleton.getInstance().allPendingJobs

            //only 100 job is allowed
            if (allPendingJobs.size < 95)   com.ilisium.fireapp.utils.JobSchedulerSingleton.getInstance()
                .schedule(builder.build())
        }

        fun cancel(messageId: String?) {
            val jobId =   com.ilisium.fireapp.utils.RealmHelper.getInstance().getJobId(messageId, false)
            if (jobId != -1) {
                  com.ilisium.fireapp.utils.JobSchedulerSingleton.getInstance().cancel(jobId)
            }
        }
    }
}