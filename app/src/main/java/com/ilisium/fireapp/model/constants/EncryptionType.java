package com.ilisium.fireapp.model.constants;

public class EncryptionType {
    public static final String NONE = "none";
    public static final String AES = "AES";
    public static final String E2E = "E2E";
}
