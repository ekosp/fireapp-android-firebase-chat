package com.ilisium.fireapp.events

class UpdateCallDurationEvent(val duration: Long)
