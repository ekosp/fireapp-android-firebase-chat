package com.ilisium.fireapp.adapters.messaging.holders

import android.content.Context
import android.view.View
import com.aghajari.emojiview.view.AXEmojiTextView
import com.ilisium.fireapp.R
import com.ilisium.fireapp.adapters.messaging.holders.base.BaseSentHolder
import com.ilisium.fireapp.model.realms.Message
import com.ilisium.fireapp.model.realms.User


// sent message with type text
class SentTextHolder(context: Context, itemView: View) : BaseSentHolder(context, itemView) {
    private var tvMessageContent: AXEmojiTextView = itemView.findViewById(R.id.tv_message_content)

    override fun bind(message:   com.ilisium.fireapp.model.realms.Message, user:   com.ilisium.fireapp.model.realms.User) {
        super.bind(message, user)
        tvMessageContent.text = message.content
    }

}

