package com.ilisium.fireapp.adapters.messaging

import com.ilisium.fireapp.model.realms.RealmContact

interface ContactHolderInteraction {
    fun onMessageClick(contact:   com.ilisium.fireapp.model.realms.RealmContact)
    fun onAddContactClick(contact:   com.ilisium.fireapp.model.realms.RealmContact)
}