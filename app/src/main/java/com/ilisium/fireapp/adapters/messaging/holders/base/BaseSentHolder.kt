package com.ilisium.fireapp.adapters.messaging.holders.base

import android.content.Context
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ilisium.fireapp.R
import com.ilisium.fireapp.model.realms.Message
import com.ilisium.fireapp.model.realms.QuotedMessage
import com.ilisium.fireapp.model.realms.User
import com.ilisium.fireapp.utils.AdapterHelper
import com.ilisium.fireapp.utils.MessageTypeHelper

open class BaseSentHolder(context: Context, itemView: View) : BaseHolder(context,itemView) {

    var messageStatImg:ImageView? = itemView.findViewById(R.id.message_stat_img)


    override fun bind(message:   com.ilisium.fireapp.model.realms.Message, user:   com.ilisium.fireapp.model.realms.User) {
        super.bind(message, user)


        //imgStat (received or read)
        messageStatImg?.setImageResource(  com.ilisium.fireapp.utils.AdapterHelper.getMessageStatDrawable(message.messageStat))


    }




}

