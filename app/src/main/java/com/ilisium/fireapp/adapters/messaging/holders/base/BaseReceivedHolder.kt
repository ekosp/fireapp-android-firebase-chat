package com.ilisium.fireapp.adapters.messaging.holders.base

import android.content.Context
import android.view.View
import android.widget.TextView
import com.ilisium.fireapp.R
import com.ilisium.fireapp.model.realms.Message
import com.ilisium.fireapp.model.realms.User
import com.ilisium.fireapp.utils.ListUtil

// received message holders
open class BaseReceivedHolder(context: Context, itemView: View) : BaseHolder(context, itemView) {
    var userName: TextView? = itemView.findViewById(R.id.tv_username_group)



    override fun bind(message:   com.ilisium.fireapp.model.realms.Message, user:   com.ilisium.fireapp.model.realms.User) {
        super.bind(message, user)

        if (user.isGroupBool && userName != null) {
            userName?.visibility = View.VISIBLE
            val fromId = message.fromId
            val userById =   com.ilisium.fireapp.utils.ListUtil.getUserById(fromId, user.getGroup().getUsers())
            if (userById != null) {
                val name = userById.userName
                if (name != null) userName?.text = name
            } else {
                userName?.text = message.fromPhone
            }
        }

    }



}
