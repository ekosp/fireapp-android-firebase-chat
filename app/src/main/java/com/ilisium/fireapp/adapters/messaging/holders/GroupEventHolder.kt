package com.ilisium.fireapp.adapters.messaging.holders

import android.content.Context
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ilisium.fireapp.R
import com.ilisium.fireapp.adapters.messaging.holders.base.BaseHolder
import com.ilisium.fireapp.model.realms.GroupEvent
import com.ilisium.fireapp.model.realms.Message
import com.ilisium.fireapp.model.realms.User

 class GroupEventHolder(context: Context, itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val tvGroupEvent: TextView = itemView.findViewById(R.id.tv_group_event)

     fun bind(message:   com.ilisium.fireapp.model.realms.Message, user:   com.ilisium.fireapp.model.realms.User){
         tvGroupEvent.text =   com.ilisium.fireapp.model.realms.GroupEvent.extractString(message.content, user.group.users)
     }


}