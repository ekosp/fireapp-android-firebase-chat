package com.ilisium.fireapp.adapters.messaging

import androidx.lifecycle.LiveData
import com.ilisium.fireapp.model.AudibleState

interface AudibleBase {
    var audibleState: LiveData<Map<String, AudibleState>>?
    var audibleInteraction:AudibleInteraction?
}