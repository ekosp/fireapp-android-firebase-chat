package com.ilisium.fireapp.adapters.messaging

import android.view.View
import com.ilisium.fireapp.model.realms.Message

interface Interaction {
    fun onContainerViewClick(pos:Int,itemView:View,message:   com.ilisium.fireapp.model.realms.Message)
    fun onItemViewClick(pos:Int,itemView:View,message:   com.ilisium.fireapp.model.realms.Message)
    fun onLongClick(pos:Int,itemView:View,message:   com.ilisium.fireapp.model.realms.Message)
    fun onProgressButtonClick(pos:Int,itemView:View,message:   com.ilisium.fireapp.model.realms.Message)
    fun onQuotedMessageClick(pos:Int,itemView:View,message:   com.ilisium.fireapp.model.realms.Message)

}