package com.ilisium.fireapp.adapters.messaging

import com.ilisium.fireapp.model.realms.Message

interface AudibleInteraction {
    fun onSeek(message:   com.ilisium.fireapp.model.realms.Message, progress:Int, max:Int)
}