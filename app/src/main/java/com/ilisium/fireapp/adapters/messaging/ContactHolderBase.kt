package com.ilisium.fireapp.adapters.messaging

interface ContactHolderBase {
    var contactHolderInteraction: ContactHolderInteraction?
}