package com.ilisium.fireapp.adapters.messaging.holders

import android.content.Context
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ilisium.fireapp.R
import com.ilisium.fireapp.adapters.messaging.holders.base.BaseSentHolder
import com.ilisium.fireapp.model.realms.Message
import com.ilisium.fireapp.model.realms.User

class SentDeletedMessageHolder(context: Context, itemView: View) : BaseSentHolder(context,itemView)

