package com.ilisium.fireapp.services

import android.content.Intent
import android.os.Handler
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.ilisium.fireapp.activities.calling.model.CallType
import com.ilisium.fireapp.model.constants.*
import com.ilisium.fireapp.model.realms.*
import com.ilisium.fireapp.utils.*
import com.ilisium.fireapp.utils.enc.DecryptionHelper
import com.ilisium.fireapp.utils.enc.EncryptionHelper
import com.ilisium.fireapp.utils.enc.MessageDecryptor
import com.ilisium.fireapp.utils.network.FireManager
import com.ilisium.fireapp.utils.network.FireManager.Companion.isLoggedIn
import com.ilisium.fireapp.utils.network.FireManager.Companion.uid
import com.ilisium.fireapp.utils.update.UpdateChecker
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.Main
import java.lang.Runnable

class MyFCMService : FirebaseMessagingService() {
    private var fireManager = FireManager()
    private var disposables = CompositeDisposable()
    private lateinit var newMessageHandler: NewMessageHandler

    private val parentJob = SupervisorJob()
    private val scope = CoroutineScope(Main + parentJob)


    private val updateChecker: UpdateChecker by lazy {
        UpdateChecker(this)
    }

    private val decryptionHelper: DecryptionHelper by lazy {
        DecryptionHelper()
    }

    private val messageDecryptor: MessageDecryptor by lazy {
        MessageDecryptor(decryptionHelper)
    }


    override fun onNewToken(s: String) {
        super.onNewToken(s)
        if (!isLoggedIn()) return  //if the user clears the app data or sign out we don't wan't to do nothing


          com.ilisium.fireapp.utils.SharedPreferencesManager.setTokenSaved(false)
          com.ilisium.fireapp.utils.ServiceHelper.saveToken(this, s)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        if (FireManager.isLoggedIn()
                .not()
        ) return  //if the user clears the app data or sign out we don't wan't to do nothing
        if (updateChecker.needsUpdate()) return

        val mainHandler = Handler(mainLooper)
        //run on main thread
        val myRunnable = Runnable {
            newMessageHandler = NewMessageHandler(this, fireManager, messageDecryptor, disposables)



            if (remoteMessage.data.containsKey("event")) {
                //this will called when something is changed in group.
                // like member removed,added,admin changed, group info changed...
                when {
                    remoteMessage.data["event"] == "group_event" -> {
                        handleGroupEvent(remoteMessage)
                    }
                    remoteMessage.data["event"] == "new_group" -> {
                        handleNewGroup(remoteMessage)
                    }
                    remoteMessage.data["event"] == "message_deleted" -> {
                        handleDeletedMessage(remoteMessage)
                    }
                    remoteMessage.data["event"] == "new_call" -> {
                        handleNewCall(remoteMessage)
                    }

                    remoteMessage.data["event"] == "logout" -> {
                        val newDeviceId = remoteMessage.data["deviceId"]

                        if (newDeviceId != null && newDeviceId != DeviceId.id) {
                            LocalBroadcastManager.getInstance(this)
                                .sendBroadcast(Intent(  com.ilisium.fireapp.utils.IntentUtils.ACTION_LOGOUT))
                            FireManager.logout()
                              com.ilisium.fireapp.utils.NotificationHelper(this).fireUserLoggedOutNotification()
                        }
                    }

                }
            } else {
                if (remoteMessage.data.containsKey(  com.ilisium.fireapp.model.constants.DBConstants.MESSAGE_ID)) {
                    handleNewMessage(remoteMessage)
                }
            }
        }
        mainHandler.post(myRunnable)
    }

    private fun handleNewCall(map: RemoteMessage) {
        val data = map.data

        data["callId"]?.let { callId ->


            val fromId = data["callerId"] ?: ""

            val typeInt = data["callType"]?.toIntOrNull() ?: CallType.VOICE.value
            val type = CallType.fromInt(typeInt)


            val groupId = data["groupId"] ?: ""

            val isGroupCall = type.isGroupCall()

            if (!isGroupCall && uid.isEmpty()) return@let
            if (isGroupCall && groupId.isEmpty()) return@let
            val channel = data["channel"] ?: return@let
            val groupName = data["groupName"] ?: ""

            val timestamp = data["timestamp"]?.toLongOrNull() ?: System.currentTimeMillis()
            val phoneNumber = data["phoneNumber"] ?: ""

            val isVideo = type.isVideo()

            val uid = if (isGroupCall) groupId else fromId


            var user:   com.ilisium.fireapp.model.realms.User

            val storedUser =   com.ilisium.fireapp.utils.RealmHelper.getInstance().getUser(uid)

            if (storedUser == null) {
                //make dummy user temporarily
                user =   com.ilisium.fireapp.model.realms.User().apply {
                    if (isGroupCall) {
                        this.uid = groupId!!
                        this.isGroupBool = true
                        this.userName = groupName
                        this.group =   com.ilisium.fireapp.model.realms.Group().apply {
                            this.groupId = groupId
                            this.isActive = true
                            this.setUsers(mutableListOf(  com.ilisium.fireapp.utils.SharedPreferencesManager.getCurrentUser()))
                        }

                    } else {
                        this.uid = uid
                        this.phone = phoneNumber
                    }
                }
            } else {
                user = storedUser
            }

            val fireCall =   com.ilisium.fireapp.model.realms.FireCall(
                callId,
                user,
                FireCallDirection.INCOMING,
                timestamp,
                phoneNumber,
                isVideo,
                typeInt,
                channel
            )


            newMessageHandler.handleNewCall(fireCall)

        }

    }

    private fun handleNewMessage(remoteMessage: RemoteMessage) {
        val messageId = remoteMessage.data[  com.ilisium.fireapp.model.constants.DBConstants.MESSAGE_ID]

        //if message is deleted do not save it
        if (  com.ilisium.fireapp.utils.RealmHelper.getInstance().getDeletedMessage(messageId) != null) return
        if (  com.ilisium.fireapp.utils.RealmHelper.getInstance().getTempMessage(messageId) != null) return


        val isGroup = remoteMessage.data.containsKey("isGroup")
        //getting data from fcm message and convert it to a message
        val phone = remoteMessage.data[  com.ilisium.fireapp.model.constants.DBConstants.PHONE] ?: ""
        val content = remoteMessage.data[  com.ilisium.fireapp.model.constants.DBConstants.CONTENT]
        val timestamp = remoteMessage.data[  com.ilisium.fireapp.model.constants.DBConstants.TIMESTAMP]
        val type = remoteMessage.data[  com.ilisium.fireapp.model.constants.DBConstants.TYPE]?.toInt() ?: 0
        //get sender uid
        val fromId = remoteMessage.data[  com.ilisium.fireapp.model.constants.DBConstants.FROM_ID]
        val toId = remoteMessage.data[  com.ilisium.fireapp.model.constants.DBConstants.TOID]
        val metadata = remoteMessage.data[  com.ilisium.fireapp.model.constants.DBConstants.METADATA]
        val partialText = remoteMessage.data[  com.ilisium.fireapp.model.constants.DBConstants.PARTIAL_TEXT]

        //convert sent type to received
        val convertedType =   com.ilisium.fireapp.model.constants.MessageType.convertSentToReceived(type)


        val encryptionType = remoteMessage.data[  com.ilisium.fireapp.model.constants.DBConstants.ENCRYPTION_TYPE] ?:   com.ilisium.fireapp.model.constants.EncryptionType.NONE

        //if it's a group message and the message sender is the same
        if (fromId == uid) return

        //create the message
        val message =   com.ilisium.fireapp.model.realms.Message()
        message.content = content
        message.partialText = partialText
        message.timestamp = timestamp

        message.fromId = fromId
        message.type = convertedType
        message.messageId = messageId
        message.metadata = metadata
        message.toId = toId
        val chatId = if (isGroup) toId else fromId
        message.chatId = chatId
        message.isGroup = isGroup
        if (isGroup) message.fromPhone = phone
        //set default state
        message.downloadUploadStat =   com.ilisium.fireapp.model.constants.DownloadUploadStat.FAILED

        message.encryptionType = encryptionType

        //check if it's text message
        if (  com.ilisium.fireapp.model.constants.MessageType.isSentText(type)) {
            //set the state to default
            message.downloadUploadStat =   com.ilisium.fireapp.model.constants.DownloadUploadStat.DEFAULT


            //check if it's a contact
        } else if (remoteMessage.data.containsKey(  com.ilisium.fireapp.model.constants.DBConstants.CONTACT)) {
            message.downloadUploadStat =   com.ilisium.fireapp.model.constants.DownloadUploadStat.DEFAULT
            //get the json contact as String
            val contactStr = remoteMessage.data[  com.ilisium.fireapp.model.constants.DBConstants.CONTACT] ?: ""
            //convert contact numbers from JSON to ArrayList
//            val phoneNumbersList = ContactMapper.mapStringToNumbers(contactStr)
            // convert it to RealmContact and set the contact name using content
            val realmContact =   com.ilisium.fireapp.model.realms.RealmContact(
                content,
                arrayListOf(),
                contactStr
            )
            message.contact = realmContact


            //check if it's a location message
        } else if (remoteMessage.data.containsKey(  com.ilisium.fireapp.model.constants.DBConstants.LOCATION)) {


            message.downloadUploadStat =   com.ilisium.fireapp.model.constants.DownloadUploadStat.DEFAULT
            //get the json location as String
            val lat = remoteMessage.data["lat"] ?: ""
            val lng = remoteMessage.data["lng"] ?: ""
            if (lat.isNotEmpty() && lng.isNotEmpty()) {
                val name = remoteMessage.data["name"] ?: ""
                val address = remoteMessage.data["address"] ?: ""
                val location =   com.ilisium.fireapp.model.realms.RealmLocation(
                    address,
                    name,
                    lat,
                    lng
                )
                message.location = location
            }
        } else if (remoteMessage.data.containsKey(  com.ilisium.fireapp.model.constants.DBConstants.THUMB)) {
            val thumb = remoteMessage.data[  com.ilisium.fireapp.model.constants.DBConstants.THUMB]

            //Check if it's Video and set Video Duration
            if (remoteMessage.data.containsKey(  com.ilisium.fireapp.model.constants.DBConstants.MEDIADURATION)) {
                val mediaDuration = remoteMessage.data[  com.ilisium.fireapp.model.constants.DBConstants.MEDIADURATION]
                message.mediaDuration = mediaDuration
            }
            message.thumb = thumb


            //check if it's Voice Message or Audio File
        } else if (remoteMessage.data.containsKey(  com.ilisium.fireapp.model.constants.DBConstants.MEDIADURATION)
            && type ==   com.ilisium.fireapp.model.constants.MessageType.SENT_VOICE_MESSAGE || type ==   com.ilisium.fireapp.model.constants.MessageType.SENT_AUDIO
        ) {

            //set audio duration
            val mediaDuration = remoteMessage.data[  com.ilisium.fireapp.model.constants.DBConstants.MEDIADURATION]
            message.mediaDuration = mediaDuration

            //check if it's a File
        } else if (remoteMessage.data.containsKey(  com.ilisium.fireapp.model.constants.DBConstants.FILESIZE)) {
            val fileSize = remoteMessage.data[  com.ilisium.fireapp.model.constants.DBConstants.FILESIZE]
            message.fileSize = fileSize
        }

        //if the message was quoted save it and get the quoted message
        if (remoteMessage.data.containsKey("quotedMessageId")) {
            val quotedMessageId = remoteMessage.data["quotedMessageId"]
            //sometimes the message is not saved because of threads,
            //so we need to make sure that we refresh the database before checking if the message is exists
              com.ilisium.fireapp.utils.RealmHelper.getInstance().refresh()
            val quotedMessage =   com.ilisium.fireapp.utils.RealmHelper.getInstance().getMessage(quotedMessageId, chatId)
            if (quotedMessage != null)
                message.quotedMessage =   com.ilisium.fireapp.model.realms.QuotedMessage.messageToQuotedMessage(quotedMessage)
        }

        //if the message was quoted save it and get the quoted message
        if (remoteMessage.data.containsKey("statusId")) {
            val statusId = remoteMessage.data["statusId"]
            //sometimes the message is not saved because of threads,
            //so we need to make sure that we refresh the database before checking if the message is exists
              com.ilisium.fireapp.utils.RealmHelper.getInstance().refresh()
            val status =   com.ilisium.fireapp.utils.RealmHelper.getInstance().getStatus(statusId)
            if (status != null) {
                message.status = status
                val quotedMessage =   com.ilisium.fireapp.model.realms.Status.statusToMessage(status, fromId)
                quotedMessage?.fromId = uid
                quotedMessage?.chatId = fromId
                if (quotedMessage != null)
                    message.quotedMessage =   com.ilisium.fireapp.model.realms.QuotedMessage.messageToQuotedMessage(quotedMessage)


            }
            //Save it to database and fire notification
        }


        scope.launch(Main) {
            newMessageHandler.handleNewMessage(phone, message)
        }


    }

    private fun handleDeletedMessage(remoteMessage: RemoteMessage) {
        newMessageHandler.handleDeletedMessage(remoteMessage.data)
    }

    private fun handleNewGroup(remoteMessage: RemoteMessage) {
        newMessageHandler.handleNewGroup(remoteMessage.data)
    }

    private fun handleGroupEvent(remoteMessage: RemoteMessage) {
        newMessageHandler.handleGroupEvent(remoteMessage.data)
    }


    override fun onDestroy() {
        super.onDestroy()
        disposables.dispose()
        cancelCoroutineJob()
    }

    private fun cancelCoroutineJob() = try {
//        parentJob.cancel()
    } catch (e: Exception) {
    }
}

