package com.ilisium.fireapp.activities.main

import android.app.Application
import android.content.Intent
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.ilisium.fireapp.R
import com.ilisium.fireapp.common.DisposableViewModel
import com.ilisium.fireapp.activities.main.status.StatusFragmentEvent
import com.ilisium.fireapp.common.DisposableAndroidViewModel
import com.ilisium.fireapp.common.extensions.toDeffered
import com.ilisium.fireapp.extensions.setValueRx
import com.ilisium.fireapp.extensions.snapshotAtRefExists
import com.ilisium.fireapp.job.DeleteStatusJob
import com.ilisium.fireapp.model.constants.EncryptionType
import com.ilisium.fireapp.model.realms.TextStatus
import com.ilisium.fireapp.model.constants.StatusType
import com.ilisium.fireapp.model.realms.Status
import com.ilisium.fireapp.model.realms.User
import com.ilisium.fireapp.utils.*
import com.ilisium.fireapp.utils.enc.ethree.EthreeInstance
import com.ilisium.fireapp.utils.enc.ethree.EthreeRegistration
import com.ilisium.fireapp.utils.network.FireManager
import com.ilisium.fireapp.utils.update.UpdateChecker
import com.google.firebase.database.DataSnapshot
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main

class MainViewModel(private val context: Application) : DisposableAndroidViewModel(context) {

    private val realmHelper =   com.ilisium.fireapp.utils.RealmHelper.getInstance()
    var lastSyncTime = 0L

    private val _statusLiveData = MutableLiveData<StatusFragmentEvent>()
    val statusLiveData: LiveData<StatusFragmentEvent>
        get() = _statusLiveData

    private val _queryTextChange = MutableLiveData<String>()
    val queryTextChange: LiveData<String>
        get() = _queryTextChange

    fun onQueryTextChange(text: String) {
        _queryTextChange.value = text
    }

    private val encryptionType = MyApp.context().getString(R.string.encryption_type)


    companion object {
        //15Sec
        const val WAIT_TIME = 15000
    }

    fun fetchStatuses(users: List<  com.ilisium.fireapp.model.realms.User>) {

        //wait for 15 sec before re-fetching statuses
        if (lastSyncTime == 0L || System.currentTimeMillis() - lastSyncTime > WAIT_TIME) {
            viewModelScope.launch {
                try {
                    val statusesIds = mutableListOf<String>()
                    fetchImageAndVideosStatuses(users, statusesIds)
                    fetchTextStatuses(users, statusesIds)
                    realmHelper.deleteDeletedStatusesLocally(statusesIds)
                    lastSyncTime = System.currentTimeMillis()
                    updateUi()

                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }
    }

    private fun updateUi() {
        _statusLiveData.value = StatusFragmentEvent.StatusInsertedEvent()
    }


    private fun handleStatus(dataSnapshot: DataSnapshot, statusesIds: MutableList<String>) {


        if (dataSnapshot.value != null) {
            //get every status
            for (snapshot in dataSnapshot.children) {
                val userId = snapshot.ref.parent!!.key
                val statusId = snapshot.key
                val status = snapshot.getValue(  com.ilisium.fireapp.model.realms.Status::class.java)
                status!!.statusId = statusId
                status.userId = userId

                if (status.type ==   com.ilisium.fireapp.model.constants.StatusType.TEXT) {
                    val textStatus = snapshot.getValue(TextStatus::class.java)
                    textStatus!!.statusId = statusId!!
                    status.textStatus = textStatus
                }

                statusesIds.add(statusId!!)
                //check if status is exists in local database , if not save it
                if (realmHelper.getStatus(status.statusId) == null) {
                    realmHelper.saveStatus(userId, status)
                    //schedule a job after 24 hours to delete this status locally
                      com.ilisium.fireapp.job.DeleteStatusJob.schedule(userId, statusId)
                }


            }

        }
    }

    private suspend fun fetchImageAndVideosStatuses(
        users: List<  com.ilisium.fireapp.model.realms.User>,
        statusesIds: MutableList<String>
    ) {
        //add all statuses to this list to delete deleted statuses if needed
        //get current time before 24 hours (Yesterday)
        val timeBefore24Hours =   com.ilisium.fireapp.utils.TimeHelper.getTimeBefore24Hours()
        //get all user statuses that are not passed 24 hours


        val jobs = mutableListOf<Deferred<DataSnapshot>>()


        val job = viewModelScope.async {
            for (user in users!!) {
                val query =   com.ilisium.fireapp.utils.FireConstants.statusRef.child(user.uid)
                    .orderByChild("timestamp")
                    .startAt(timeBefore24Hours.toDouble())


                val dataSnapshot = query.toDeffered()

                jobs.add(dataSnapshot)
            }

        }


        job.await()
        val datasnapshots = jobs.awaitAll()
        datasnapshots.forEach {
            handleStatus(it, statusesIds)
        }


    }

    private suspend fun fetchTextStatuses(users: List<  com.ilisium.fireapp.model.realms.User>, statusesIds: MutableList<String>) {
//        val statusesIds = mutableListOf<String>()

        //add all statuses to this list to delete deleted statuses if needed
        //get current time before 24 hours (Yesterday)
        val timeBefore24Hours =   com.ilisium.fireapp.utils.TimeHelper.getTimeBefore24Hours()
        //get all user statuses that are not passed 24 hours


        val jobs = mutableListOf<Deferred<DataSnapshot>>()

        val job = viewModelScope.async {
            for (user in users!!) {
                val query =   com.ilisium.fireapp.utils.FireConstants.textStatusRef.child(user.uid)
                    .orderByChild("timestamp")
                    .startAt(timeBefore24Hours.toDouble())


                val dataSnapshot = query.toDeffered()

                jobs.add(dataSnapshot)
            }

        }


        job.await()
        val datasnapshots = jobs.awaitAll()
        datasnapshots.forEach {
            handleStatus(it, statusesIds)
        }


    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        data?.let {
            _statusLiveData.value =
                StatusFragmentEvent.OnActivityResultEvent(requestCode, resultCode, data)
        }

    }

    //if user is coming from an old version, then delete the already received messages from his db
    fun deleteOldMessagesIfNeeded() {

        if (!  com.ilisium.fireapp.utils.SharedPreferencesManager.isDeletedUnfetchedMessage()) {

            val deleteUserMessages =
                  com.ilisium.fireapp.utils.FireConstants.userMessages.child(FireManager.uid).setValueRx(null)
            val deleteDeletedMessages =
                  com.ilisium.fireapp.utils.FireConstants.deletedMessages.child(FireManager.uid).setValueRx(null)
            val deleteNewGroupsEvents =
                  com.ilisium.fireapp.utils.FireConstants.newGroups.child(FireManager.uid).setValueRx(null)
            val setDeletedOldMessagesToTrue =
                  com.ilisium.fireapp.utils.FireConstants.hasDeletedOldMessages.child(FireManager.uid).setValueRx(true)
            val completable = Completable.merge(
                arrayListOf(
                    deleteUserMessages,
                    deleteDeletedMessages,
                    deleteNewGroupsEvents
                )
            )

              com.ilisium.fireapp.utils.FireConstants.hasDeletedOldMessages.child(FireManager.uid).snapshotAtRefExists()
                .flatMapCompletable { isExists ->

                    if (isExists) {
                        return@flatMapCompletable Completable.complete()
                    }

                    return@flatMapCompletable completable


                }.andThen(setDeletedOldMessagesToTrue)
                .doOnComplete {
                      com.ilisium.fireapp.utils.SharedPreferencesManager.setDeletedUnfetchedMessage(true)
                }.subscribe({

                }, { throwable ->

                }).addTo(disposables)
        }
    }

    fun checkForUpdate(): Maybe<Boolean> {
        return UpdateChecker(context).checkForUpdate()
    }

    fun saveDeviceId() {
        viewModelScope.launch {
            try {
                FireManager().saveDeviceId(FireManager.uid)
                  com.ilisium.fireapp.utils.SharedPreferencesManager.setDeviceIdSaved(true)
            } catch (e: Exception) {

            }
        }
    }

    private fun isE2E() = encryptionType.equals(  com.ilisium.fireapp.model.constants.EncryptionType.E2E, ignoreCase = true)


    fun setupE2eIfNeeded() {
        if (isE2E() && !  com.ilisium.fireapp.utils.SharedPreferencesManager.isE2ESaved()) {
            viewModelScope.launch(IO) {
                try {
                    val ethree = EthreeInstance.initialize(this, FireManager.uid).await()
                    EthreeRegistration.registerEthree(ethree, this)
                    withContext(Main) {
                          com.ilisium.fireapp.utils.SharedPreferencesManager.setE2ESaved(true)
                    }
                } catch (e: Exception) {

                }
            }


        }
    }

    fun saveAppVersion() {
        //save app ver if it's not saved before
        if (!  com.ilisium.fireapp.utils.SharedPreferencesManager.isAppVersionSaved() ||   com.ilisium.fireapp.utils.SharedPreferencesManager.getAppVersion() !=   com.ilisium.fireapp.utils.AppVerUtil.getAppVersion(
                MyApp.context()
            )
        ) {
              com.ilisium.fireapp.utils.FireConstants.usersRef.child(FireManager.uid).child("ver")
                .setValue(  com.ilisium.fireapp.utils.AppVerUtil.getAppVersion(MyApp.context()))
                .addOnSuccessListener {   com.ilisium.fireapp.utils.SharedPreferencesManager.setAppVersionSaved(true) }
        }
    }


}


