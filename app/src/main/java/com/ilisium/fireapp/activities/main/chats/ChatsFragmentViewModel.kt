package com.ilisium.fireapp.activities.main.chats

import com.ilisium.fireapp.common.DisposableViewModel
import com.ilisium.fireapp.model.realms.User
import com.ilisium.fireapp.utils.TimeHelper
import com.ilisium.fireapp.utils.network.FireManager
import io.reactivex.rxkotlin.addTo
import java.util.*

class ChatsFragmentViewModel : DisposableViewModel() {
    private val currentDownloads = mutableListOf<String>()


    fun fetchUserImage(pos: Int, user:   com.ilisium.fireapp.model.realms.User) {
        if (user.isBroadcastBool)return
        if (  com.ilisium.fireapp.utils.TimeHelper.canFetchUserImage(Date().time, user.lastTimeFetchedImage) && !currentDownloads.contains(user.uid)) {
            currentDownloads.add(user.uid)
            FireManager.checkAndDownloadUserThumbImg(user).subscribe({image ->

            }, { throwable ->

            }).addTo(disposables)
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }

}