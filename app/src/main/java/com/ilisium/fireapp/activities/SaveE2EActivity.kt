package com.ilisium.fireapp.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.lifecycleScope
import com.ilisium.fireapp.R
import com.ilisium.fireapp.utils.SharedPreferencesManager
import com.ilisium.fireapp.utils.enc.ethree.EthreeInstance
import com.ilisium.fireapp.utils.enc.ethree.EthreeRegistration
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.withContext

class SaveE2EActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_save_e2_e)
        lifecycleScope.launchWhenCreated {
            try {
                withContext(IO) {
                    val ethree = EthreeInstance.initialize().await()
                    EthreeRegistration.registerEthree(ethree, this)
                }

                  com.ilisium.fireapp.utils.SharedPreferencesManager.setE2ESaved(true)
                startActivity(Intent(this@SaveE2EActivity,   com.ilisium.fireapp.activities.SplashActivity::class.java))
                finish()

            } catch (e: Exception) {
                withContext(Main) {
                    AlertDialog.Builder(this@SaveE2EActivity).apply {
                        setMessage(R.string.unknown_error)
                            .setPositiveButton(R.string.ok, null)
                            .show()
                    }
                }
            }
        }
    }
}