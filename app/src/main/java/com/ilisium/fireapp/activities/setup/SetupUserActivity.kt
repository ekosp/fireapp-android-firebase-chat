package com.ilisium.fireapp.activities.setup

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.ilisium.fireapp.R
import com.ilisium.fireapp.activities.main.MainActivity
import com.ilisium.fireapp.services.CompleteSetupService
import com.ilisium.fireapp.services.SetupServiceEvent
import com.ilisium.fireapp.utils.*
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_setup_user.*

class SetupUserActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setup_user)

        val username = intent.getStringExtra(  com.ilisium.fireapp.utils.IntentUtils.EXTRA_USERNAME) ?: " "
        val pickedPhotoLocalPath = intent.getStringExtra(  com.ilisium.fireapp.utils.IntentUtils.EXTRA_PICKED_PHOTO)
        val pickedBackupFile = intent.getStringExtra(  com.ilisium.fireapp.utils.IntentUtils.EXTRA_PICKED_BACKUP)
        val pickedDbUri = intent.getStringExtra(  com.ilisium.fireapp.utils.IntentUtils.EXTRA_DB_FILE_URI)



        if (!CompleteSetupService.isAcitve) {
            CompleteSetupService.startService(
                this,
                username,
                pickedPhotoLocalPath,
                pickedBackupFile,
                pickedDbUri
            )
        }

        CompleteSetupService.progressLiveData.observe(this) { progressPer ->
            progressPer?.let {
                progress.progress = it
            }
        }

        CompleteSetupService.event.observe(this) { event ->
            if (event == null) {
                return@observe
            }

            when (event) {
                is SetupServiceEvent.SetupStarted -> {
                    tv_restore_in_progress.text = getString(R.string.initializing)
                }

                is SetupServiceEvent.RestoreStarted -> {
                    tv_restore_in_progress.text = getString(R.string.initializing)
                    progress.isVisible = true
                }

                is SetupServiceEvent.SetupFinalizing -> {
                    tv_restore_in_progress.text = getString(R.string.finalizing)
                }

                is SetupServiceEvent.SetupFinished -> {
                    startMainActivity()
                }

                is SetupServiceEvent.SetupError -> {
                    AlertDialog.Builder(this).apply {
                        setTitle(R.string.error)
                        setMessage(event.error.localizedMessage)
                        setPositiveButton(R.string.ok, null)
                        show()
                    }
                }

                else -> {

                }
            }
        }
    }


    private fun showSnackbar() {
        Snackbar.make(
            findViewById(android.R.id.content),
            R.string.no_internet_connection,
            Snackbar.LENGTH_SHORT
        ).show()
    }


    private fun startMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()
    }

    override fun onResume() {
        super.onResume()
        if (  com.ilisium.fireapp.utils.SharedPreferencesManager.isCurrentUserInfoSaved()) {
            startMainActivity()
        }
    }


    companion object {
        @JvmStatic
        fun start(
            context: Context,
            userName: String,
            pickedPhotoLocalPath: String?,
            backupUri: String?,
            dbUri: String?
        ) {
            Intent(context, SetupUserActivity::class.java).apply {
                putExtra(  com.ilisium.fireapp.utils.IntentUtils.EXTRA_USERNAME, userName)
                putExtra(  com.ilisium.fireapp.utils.IntentUtils.EXTRA_PICKED_PHOTO, pickedPhotoLocalPath)
                putExtra(  com.ilisium.fireapp.utils.IntentUtils.EXTRA_PICKED_BACKUP, backupUri)
                putExtra(  com.ilisium.fireapp.utils.IntentUtils.EXTRA_DB_FILE_URI, dbUri)
                context.startActivity(this)
            }
        }
    }
}

