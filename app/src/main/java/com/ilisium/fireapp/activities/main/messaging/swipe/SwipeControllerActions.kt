package com.ilisium.fireapp.activities.main.messaging.swipe

interface SwipeControllerActions {

    fun showReplyUI(position: Int)
}