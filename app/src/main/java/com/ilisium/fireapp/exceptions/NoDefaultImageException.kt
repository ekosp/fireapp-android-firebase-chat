package com.ilisium.fireapp.exceptions

class NoDefaultImageException:Throwable("user did not upload default user profile photo")