package com.ilisium.fireapp.interfaces;

public interface StatusFragmentCallbacks {
    void openCamera();

    void fetchStatuses();
}
