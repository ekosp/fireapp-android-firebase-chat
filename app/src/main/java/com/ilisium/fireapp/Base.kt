package com.ilisium.fireapp

import io.reactivex.disposables.CompositeDisposable

interface Base {
    val disposables:CompositeDisposable
}