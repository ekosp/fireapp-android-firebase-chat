package com.ilisium.fireapp.utils.enc

import com.ilisium.fireapp.R
import com.ilisium.fireapp.model.constants.EncryptionType
import com.ilisium.fireapp.model.realms.Message
import com.ilisium.fireapp.utils.MyApp.Companion.context

object EncryptionTypeUseCase {
     fun getEncryptionType(message:   com.ilisium.fireapp.model.realms.Message): String? {
        val encryptionTypeSetting =
            context().getString(R.string.encryption_type)
        return if (message.isGroup && !encryptionTypeSetting.equals(
                  com.ilisium.fireapp.model.constants.EncryptionType.NONE,
                ignoreCase = true
            )
        ) {
              com.ilisium.fireapp.model.constants.EncryptionType.AES
        } else encryptionTypeSetting
    }
}