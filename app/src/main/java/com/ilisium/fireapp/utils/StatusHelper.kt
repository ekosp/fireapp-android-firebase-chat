package com.ilisium.fireapp.utils

import com.ilisium.fireapp.model.constants.MessageType
import com.ilisium.fireapp.model.constants.StatusType
import com.ilisium.fireapp.model.realms.Status

object StatusHelper {
    fun getStatusTypeDrawable(statusType: Int): Int {
        return   com.ilisium.fireapp.utils.MessageTypeHelper.getMessageTypeDrawable(mapStatusTypeToMessageType(statusType))
    }

    fun getStatusContent(status:   com.ilisium.fireapp.model.realms.Status): String {
        var type = mapStatusTypeToMessageType(status.type)
        return   com.ilisium.fireapp.utils.MessageTypeHelper.getTypeText(type)
    }

     fun mapStatusTypeToMessageType(statusType: Int): Int {
        return when (statusType) {
              com.ilisium.fireapp.model.constants.StatusType.IMAGE -> {
                  com.ilisium.fireapp.model.constants.MessageType.SENT_IMAGE
            }
              com.ilisium.fireapp.model.constants.StatusType.VIDEO -> {
                  com.ilisium.fireapp.model.constants.MessageType.SENT_VIDEO
            }
            else -> {
                  com.ilisium.fireapp.model.constants.MessageType.SENT_TEXT
            }
        }
    }
}