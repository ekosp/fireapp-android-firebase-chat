package com.ilisium.fireapp.utils.enc

import com.ilisium.fireapp.model.constants.EncryptionType
import com.ilisium.fireapp.utils.enc.aes.AESCrypto
import com.ilisium.fireapp.utils.enc.ethree.EthreeHelper
import kotlinx.coroutines.CoroutineScope

class EncryptionHelper {

    private val aesCrypto:   com.ilisium.fireapp.utils.enc.aes.AESCrypto by lazy {
          com.ilisium.fireapp.utils.enc.aes.AESCrypto()
    }

    suspend fun encrypt(
        scope: CoroutineScope,
        singleUidOrMultiple: SingleUidOrMultiple,
        message: String,
        encryptionType: String
    ): String {
        return when {
            encryptionType.equals(
                  com.ilisium.fireapp.model.constants.EncryptionType.AES,
                ignoreCase = true
            ) -> aesCrypto.encryptPlainTextWithRandomIV(message)
            encryptionType.equals(  com.ilisium.fireapp.model.constants.EncryptionType.E2E, ignoreCase = true) -> {
                if (singleUidOrMultiple.uids != null) {
                    EthreeHelper.encryptMessage(scope, singleUidOrMultiple.uids!!, message)
                } else {
                    EthreeHelper.encryptMessage(scope, singleUidOrMultiple.uid!!, message)
                }
            }
            else -> message
        }
    }


}