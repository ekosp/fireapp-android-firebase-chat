package com.ilisium.fireapp.utils

import com.ilisium.fireapp.model.ExpandableContact
import com.ilisium.fireapp.model.realms.PhoneNumber
import io.realm.RealmList

object ContactMapper {
    const val CONTACT_SEPARATOR = ",,,"

    fun mapNumbersToString(numbers: RealmList<  com.ilisium.fireapp.model.realms.PhoneNumber>) =
        numbers.map { it.number }.joinToString(CONTACT_SEPARATOR)

    fun mapNumbersToString(numbers: ArrayList<  com.ilisium.fireapp.model.realms.PhoneNumber>) =
        numbers.map { it.number }.joinToString(CONTACT_SEPARATOR)

    fun mapStringToNumbers(numbersString: String): List<  com.ilisium.fireapp.model.realms.PhoneNumber> {
        if (numbersString.isEmpty()) return listOf()

        val foundNumbers = numbersString.split(CONTACT_SEPARATOR)
        if (foundNumbers.isNullOrEmpty()) {
            return listOf()
        }

        return foundNumbers.map {   com.ilisium.fireapp.model.realms.PhoneNumber(it) }
    }

}