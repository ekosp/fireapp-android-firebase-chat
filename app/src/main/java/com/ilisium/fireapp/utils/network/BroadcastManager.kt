package com.ilisium.fireapp.utils.network

import com.ilisium.fireapp.extensions.observeSingleValueEvent
import com.ilisium.fireapp.extensions.setValueRx
import com.ilisium.fireapp.extensions.updateChildrenRx
import com.ilisium.fireapp.model.constants.DBConstants
import com.ilisium.fireapp.model.realms.Broadcast
import com.ilisium.fireapp.model.realms.User
import com.ilisium.fireapp.utils.FireConstants
import com.ilisium.fireapp.utils.MyApp
import com.ilisium.fireapp.utils.RealmHelper
import com.ilisium.fireapp.utils.SharedPreferencesManager

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ServerValue
import com.google.firebase.database.ValueEventListener
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.realm.RealmList
import java.util.*

class BroadcastManager {
    //this will create a new group and add users to this group

    fun createNewBroadcast(broadcastName: String, selectedUsers: List<  com.ilisium.fireapp.model.realms.User>): Single<  com.ilisium.fireapp.model.realms.User> {
        //generate broadcastId
        val broadcastId =   com.ilisium.fireapp.utils.FireConstants.broadcastsRef.push().key!!
        //generate temp group image
        val result: MutableMap<String, Any> = HashMap()
        val broadcastInfo: MutableMap<String, Any> = HashMap()
        //set timeCreated
        broadcastInfo[  com.ilisium.fireapp.model.constants.DBConstants.TIMESTAMP] = ServerValue.TIMESTAMP
        //set whom created the group
        broadcastInfo["createdBy"] =   com.ilisium.fireapp.utils.SharedPreferencesManager.getPhoneNumber()
        //set onlyAdminsCanPost by default to false
        val usersMap =   com.ilisium.fireapp.model.realms.User.toMap(selectedUsers, true)
        broadcastInfo["name"] = broadcastName
        result["info"] = broadcastInfo
        result["users"] = usersMap
        return Single.create { emitter ->
              com.ilisium.fireapp.utils.FireConstants.broadcastsRef.child(broadcastId).setValueRx(result).subscribe({
                val broadcastUser = createBroadcastLocally(broadcastName, selectedUsers, broadcastId, Date().time)
                emitter.onSuccess(broadcastUser)
            }, {
                emitter.onError(it)
            })
        }


    }

    private fun createBroadcastLocally(broadcastName: String, selectedUsers: List<  com.ilisium.fireapp.model.realms.User>, broadcastId: String, timestamp: Long):   com.ilisium.fireapp.model.realms.User {
        val broadcastUser =   com.ilisium.fireapp.model.realms.User()
        broadcastUser.userName = broadcastName
        broadcastUser.status = ""
        broadcastUser.phone = ""
        val list = RealmList<  com.ilisium.fireapp.model.realms.User>()
        list.addAll(selectedUsers)
        val broadcast =   com.ilisium.fireapp.model.realms.Broadcast()
        broadcast.broadcastId = broadcastId
        broadcast.users = list
        broadcast.timestamp = timestamp
        broadcast.createdByNumber =   com.ilisium.fireapp.utils.SharedPreferencesManager.getPhoneNumber()
        broadcastUser.broadcast = broadcast
        broadcastUser.isBroadcastBool = true
        broadcastUser.uid = broadcastId
          com.ilisium.fireapp.utils.RealmHelper.getInstance().saveObjectToRealm(broadcastUser)
          com.ilisium.fireapp.utils.RealmHelper.getInstance().saveEmptyChat(broadcastUser)
        return broadcastUser
    }


    fun deleteBroadcast(broadcastId: String): Completable {
        return   com.ilisium.fireapp.utils.FireConstants.broadcastsRef.child(broadcastId!!).setValueRx(null).doOnComplete {
              com.ilisium.fireapp.utils.RealmHelper.getInstance().deleteBroadcast(broadcastId)
        }
    }


    fun removeBroadcastMember(broadcastId: String, userToDeleteUid: String): Completable {
        return   com.ilisium.fireapp.utils.FireConstants.broadcastsRef.child(broadcastId).child("users").child(userToDeleteUid).setValueRx(null).doOnComplete {
              com.ilisium.fireapp.utils.RealmHelper.getInstance().deleteBroadcastMember(broadcastId, userToDeleteUid)
        }
    }


    fun addParticipant(broadcastId: String, selectedUsers: ArrayList<  com.ilisium.fireapp.model.realms.User>): Completable {
        val map: MutableMap<String, Any> = HashMap()

        for (selectedUser in selectedUsers) {
            map[selectedUser.uid] = false
        }
        return   com.ilisium.fireapp.utils.FireConstants.broadcastsRef.child(broadcastId).child("users").updateChildrenRx(map).doOnComplete {
            for (selectedUser in selectedUsers) {
                  com.ilisium.fireapp.utils.RealmHelper.getInstance().addUserToBroadcast(broadcastId, selectedUser)
            }
        }
    }


    fun changeBroadcastName(broadcastId: String, newTitle: String): Completable {
        return   com.ilisium.fireapp.utils.FireConstants.broadcastsRef.child(broadcastId).child("info").child("name").setValueRx(newTitle).doOnComplete {
              com.ilisium.fireapp.utils.RealmHelper.getInstance().changeBroadcastName(broadcastId, newTitle)
        }
    }

    fun fetchBroadcast(broadcastId: String): Observable<  com.ilisium.fireapp.model.realms.User> {
        //get only broadcasts that created by this user
        return   com.ilisium.fireapp.utils.FireConstants.broadcastsRef.child(broadcastId).observeSingleValueEvent().flatMapObservable { dataSnapshot ->


            val info = dataSnapshot.child("info")
            val usersSnapshot = dataSnapshot.child("users")
            val broadcastUserIds = getBroadcastUsersIds(usersSnapshot).filterNotNull()

            return@flatMapObservable UserByIdsDataSource.getUsersByIds(broadcastUserIds).map { Pair(it, info) }
        }.map { pair ->
            val users = pair.first
            val info = pair.second
            val broadcastName = info.child("name").getValue(String::class.java) ?: ""
            val timestampVal = info.child("timestamp").getValue(Long::class.java)

            val timestamp = timestampVal ?: Date().time

            return@map createBroadcastLocally(broadcastName, users, broadcastId, timestamp)

        }
    }


    private fun getBroadcastUsersIds(usersSnapshot: DataSnapshot): List<String?> {
        val uids: MutableList<String?> = ArrayList()
        for (child in usersSnapshot.children) {
            val uid = child.key
            //don't add current user to broadcast
            if (uid != FireManager.uid) {
                uids.add(uid)
            }
        }
        return uids
    }


    fun fetchBroadcasts(uid: String): Observable<List<  com.ilisium.fireapp.model.realms.User>> {
        //get only broadcasts that created by this user
        return   com.ilisium.fireapp.utils.FireConstants.broadcastsByUser.child(uid).orderByValue().equalTo(true).observeSingleValueEvent().map { dataSnapshot ->
            if (dataSnapshot.hasChildren().not()) {
                return@map listOf<String>()
            } else {
                return@map dataSnapshot.children.map { it.key!! }
            }

        }.flatMapObservable { broadcastsIds ->

            val observablesList = broadcastsIds.map { fetchBroadcast(it) }
            return@flatMapObservable Observable.merge(observablesList).toList().toObservable()

        }
    }


}