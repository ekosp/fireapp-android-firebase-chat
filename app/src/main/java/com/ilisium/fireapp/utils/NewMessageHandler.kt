package com.ilisium.fireapp.utils

import android.content.Context
import com.ilisium.fireapp.common.extensions.toDeffered
import com.ilisium.fireapp.model.realms.TempMessage
import com.ilisium.fireapp.services.CallingService
import com.ilisium.fireapp.utils.enc.MessageDecryptor
import com.ilisium.fireapp.utils.network.CallsManager
import com.ilisium.fireapp.utils.network.FireManager
import com.ilisium.fireapp.utils.network.FireManager.Companion.uid
import com.ilisium.fireapp.utils.network.GroupManager
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.withContext

class NewMessageHandler(
    private val context: Context,
    private val fireManager: FireManager,
    private val messageDecryptor: MessageDecryptor,
    private val disposables: CompositeDisposable
) {
    //fire notification
    suspend fun handleNewMessage(phone: String, message:   com.ilisium.fireapp.model.realms.Message) {
        withContext(Main) {
            try {

                //if message is already exists don't save it
                if (  com.ilisium.fireapp.utils.RealmHelper.getInstance()
                        .getTempMessage(message.messageId) != null ||   com.ilisium.fireapp.utils.RealmHelper.getInstance()
                        .getMessage(message.messageId) != null
                ) {
                    return@withContext
                }
                val chatId = message.chatId


                //if unknown number contacted us ,we want to download his data and save it in local db
                if (!message.isGroup &&   com.ilisium.fireapp.utils.RealmHelper.getInstance().getUser(chatId) == null)
                    fireManager.fetchAndSaveUserByPhone(phone).subscribe()
                        .addTo(disposables) //CAN WE ADD THIS TO DISPOSABLES

                //check if auto download is enabled for current network type
                val canDownload =   com.ilisium.fireapp.utils.SharedPreferencesManager.canDownload(
                    message.type,
                      com.ilisium.fireapp.utils.NetworkHelper.getCurrentNetworkType(context)
                )
                if (canDownload) {
                    //set state to downloading
                    message.downloadUploadStat =   com.ilisium.fireapp.model.constants.DownloadUploadStat.LOADING
                }

                //save message to database
                if (message.isGroup) {
                    val user =   com.ilisium.fireapp.utils.RealmHelper.getInstance().getUser(chatId)
                    if (user != null) {
                        saveMessageAndUpdateCount(message, user)
                    }
                } else {
                    saveMessageAndUpdateCount(message, phone)
                }

                //start auto download
                if (canDownload) {
                      com.ilisium.fireapp.utils.ServiceHelper.startNetworkRequest(context, message.messageId, chatId)
                }


                val messageId = message.messageId
                if (!message.isGroup)
                    setMessageStat(messageId, chatId)


                //if the current activity is not alive OR the activity chatId is not the same with the current chat id
                //then fire notification
                if (chatId != MyApp.currentChatId) {
                      com.ilisium.fireapp.utils.NotificationHelper(context).fireNotification(message.chatId)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }


    }

    private suspend fun saveMessageAndUpdateCount(
        encryptedMessage:   com.ilisium.fireapp.model.realms.Message,
        phone: String
    ) {
        val tempMessage =   com.ilisium.fireapp.model.realms.TempMessage.mapMessageToTempMessage(encryptedMessage)
          com.ilisium.fireapp.utils.RealmHelper.getInstance().saveObjectToRealm(tempMessage)

        encryptedMessage.thumb = fetchThumbIfNeeded(encryptedMessage)
        encryptedMessage.content = fetchContentIfNeeded(encryptedMessage)

        var decryptedMessage:   com.ilisium.fireapp.model.realms.Message
        withContext(IO) {
            decryptedMessage = messageDecryptor.decryptMessage(encryptedMessage, this)
        }


          com.ilisium.fireapp.utils.RealmHelper.getInstance().deleteTempMessage(tempMessage.messageId)


        //set message as seen if same chat is already open
        if (  com.ilisium.fireapp.utils.NotificationHelper.isBelowApi24() && MyApp.currentChatId == decryptedMessage.chatId) {
            decryptedMessage.isSeen = true
        }

        //save message
          com.ilisium.fireapp.utils.RealmHelper.getInstance().saveMessageFromFCM(context, decryptedMessage, phone)

        //if the current activity is not alive OR the activity chatId is not the same with the current chat id
        //then increment unread count
        if (MyApp.currentChatId != decryptedMessage.chatId) {
              com.ilisium.fireapp.utils.RealmHelper.getInstance()
                .saveUnreadMessage(decryptedMessage.messageId, decryptedMessage.chatId)
        }


    }

    private suspend fun saveMessageAndUpdateCount(
        encryptedMessage:   com.ilisium.fireapp.model.realms.Message,
        user:   com.ilisium.fireapp.model.realms.User
    ) {


        val tempMessage =   com.ilisium.fireapp.model.realms.TempMessage.mapMessageToTempMessage(encryptedMessage)
          com.ilisium.fireapp.utils.RealmHelper.getInstance().saveObjectToRealm(tempMessage)


        var decryptedMessage:   com.ilisium.fireapp.model.realms.Message
        encryptedMessage.thumb = fetchThumbIfNeeded(encryptedMessage)
        encryptedMessage.content = fetchContentIfNeeded(encryptedMessage)
        withContext(IO) {
            decryptedMessage = messageDecryptor.decryptMessage(encryptedMessage, this)
        }


          com.ilisium.fireapp.utils.RealmHelper.getInstance().deleteTempMessage(tempMessage.messageId)

        //set message as seen if same chat is already open
        if (  com.ilisium.fireapp.utils.NotificationHelper.isBelowApi24() && MyApp.currentChatId == decryptedMessage.chatId) {
            decryptedMessage.isSeen = true
        }
        //save message
          com.ilisium.fireapp.utils.RealmHelper.getInstance().saveMessageFromFCM(decryptedMessage, user)

        //if the current activity is not alive OR the activity chatId is not the same with the current chat id
        //then increment unread count
        if (MyApp.currentChatId != decryptedMessage.chatId) {
              com.ilisium.fireapp.utils.RealmHelper.getInstance()
                .saveUnreadMessage(decryptedMessage.messageId, decryptedMessage.chatId)
        }


    }

    //update the sender with message state (received)
    private fun setMessageStat(messageId: String, chatId: String) {
          com.ilisium.fireapp.utils.ServiceHelper.startUpdateMessageStatRequest(
            context,
            messageId,
            uid,
            chatId,
              com.ilisium.fireapp.model.constants.MessageStat.RECEIVED
        )
    }

    fun handleDeletedMessage(map: Map<String, Any>) {
        (map["messageId"] as? String)?.let { messageId ->
            //if it's already exists do nothing
            if (  com.ilisium.fireapp.utils.RealmHelper.getInstance().getDeletedMessage(messageId) != null) return
            if (  com.ilisium.fireapp.utils.RealmHelper.getInstance().getTempMessage(messageId) != null) return


            val message =   com.ilisium.fireapp.utils.RealmHelper.getInstance().getMessage(messageId)
              com.ilisium.fireapp.utils.RealmHelper.getInstance().setMessageDeleted(messageId)
              com.ilisium.fireapp.utils.RealmHelper.getInstance().deleteTempMessage(messageId)
            if (message != null) {
                if (message.downloadUploadStat ==   com.ilisium.fireapp.model.constants.DownloadUploadStat.LOADING) {
                    if (  com.ilisium.fireapp.model.constants.MessageType.isSentType(message.type)) {
                        DownloadManager.cancelUpload(message.messageId)
                    } else DownloadManager.cancelDownload(message.messageId)
                }
                  com.ilisium.fireapp.utils.NotificationHelper(context).messageDeleted(message)
            }
        }
    }

    fun handleGroupEvent(map: Map<String, Any>) {
        val groupId = map["groupId"] as? String
        val eventId = map["eventId"] as? String
        val contextStart = map["contextStart"] as? String
        val eventType = (map["eventType"] as? String ?: "0").toInt()
        val contextEnd = map["contextEnd"] as? String
        //if this event was by the admin himself  OR if the event already exists do nothing
        if (contextStart ==   com.ilisium.fireapp.utils.SharedPreferencesManager.getPhoneNumber() ||   com.ilisium.fireapp.utils.RealmHelper.getInstance()
                .getMessage(eventId) != null
        ) {
            return
        }

        val groupEvent =   com.ilisium.fireapp.model.realms.GroupEvent(
            contextStart,
            eventType,
            contextEnd,
            eventId
        )
        val pendingGroupJob =   com.ilisium.fireapp.model.realms.PendingGroupJob(
            groupId,
              com.ilisium.fireapp.model.constants.PendingGroupTypes.CHANGE_EVENT,
            groupEvent
        )
          com.ilisium.fireapp.utils.RealmHelper.getInstance().saveObjectToRealm(pendingGroupJob)
          com.ilisium.fireapp.utils.ServiceHelper.updateGroupInfo(context, groupId, groupEvent)
    }

    fun handleNewGroup(map: Map<String, Any>) {
        val groupId = map["groupId"] as? String
        //if it's already exists do nothing
        if (  com.ilisium.fireapp.utils.RealmHelper.getInstance().getPendingGroupJob(groupId) != null) return

        val user =   com.ilisium.fireapp.utils.RealmHelper.getInstance().getUser(groupId)


        //if the group is not exists,fetch and download it
        if (user == null) {
            val pendingGroupJob =
                  com.ilisium.fireapp.model.realms.PendingGroupJob(
                    groupId,
                      com.ilisium.fireapp.model.constants.PendingGroupTypes.CREATION_EVENT,
                    null
                )
              com.ilisium.fireapp.utils.RealmHelper.getInstance().saveObjectToRealm(pendingGroupJob)
              com.ilisium.fireapp.utils.ServiceHelper.fetchAndCreateGroup(context, groupId)
        } else {
            val users = user.group.users
            val userById =   com.ilisium.fireapp.utils.ListUtil.getUserById(uid, users)

            //if the group is not active or the group does not contain current user
            // then fetch and download it and set it as Active
            if (!user.group.isActive || !users.contains(userById)) {
                val pendingGroupJob =
                      com.ilisium.fireapp.model.realms.PendingGroupJob(
                        groupId,
                          com.ilisium.fireapp.model.constants.PendingGroupTypes.CREATION_EVENT,
                        null
                    )
                  com.ilisium.fireapp.utils.RealmHelper.getInstance().saveObjectToRealm(pendingGroupJob)
                  com.ilisium.fireapp.utils.ServiceHelper.fetchAndCreateGroup(context, groupId)
            }
        }
    }

    fun handleNewCall(fireCall:   com.ilisium.fireapp.model.realms.FireCall) {

        val storedFirecall =   com.ilisium.fireapp.utils.RealmHelper.getInstance().getFireCall(fireCall.callId)
        if (storedFirecall != null) return

        if (MyApp.isIsCallActive ||   com.ilisium.fireapp.utils.TimeHelper.isTimePassedBySeconds(
                System.currentTimeMillis(),
                fireCall.timestamp,
                CallsManager.CALL_TIEMOUT_SECONDS
            )
        ) {
            fireCall.direction =   com.ilisium.fireapp.model.constants.FireCallDirection.MISSED
              com.ilisium.fireapp.utils.RealmHelper.getInstance().saveObjectToRealm(fireCall)
              com.ilisium.fireapp.utils.NotificationHelper(context).createMissedCallNotification(
                fireCall.user,
                fireCall.phoneNumber
            )

            return
        }




        if (fireCall.isGroupCall) {
            if (fireCall.user != null) {
                GroupManager().fetchAndCreateGroup(fireCall.user.uid).subscribe({}, { })
                    .addTo(disposables)
            }

        } else {
            if (fireCall.user != null) {
                FireManager.fetchUserByUid(fireCall.user.uid).subscribe({}, { error -> })
                    .addTo(disposables)
            }
        }




          com.ilisium.fireapp.utils.RealmHelper.getInstance().saveObjectToRealm(fireCall)
        context.startService(
            CallingService.getStartIntent(
                context,
                fireCall,
                  com.ilisium.fireapp.utils.IntentUtils.NOTIFICATION_ACTION_START_INCOMING
            )
        )


    }

    /*
    * this will fetch UnDownloaded data like thumb and videoThumb
    * since they're encrypted and exceeds FCM Payload Size 4096 Bytes
    * and FCM will empty these out.
    * */
    private suspend fun fetchThumbIfNeeded(message:   com.ilisium.fireapp.model.realms.Message): String? {
        if (message.type ==   com.ilisium.fireapp.model.constants.MessageType.RECEIVED_IMAGE ||
            message.type ==   com.ilisium.fireapp.model.constants.MessageType.RECEIVED_VIDEO &&
            message.thumb.isNullOrEmpty()
        ) {
            val dataSnapshot =
                  com.ilisium.fireapp.utils.FireConstants.userMessages.child(FireManager.uid).child(message.messageId)
                    .child(  com.ilisium.fireapp.model.constants.DBConstants.THUMB).toDeffered().await()
            return dataSnapshot.value as? String
        }

        return message.thumb


    }

    private suspend fun fetchContentIfNeeded(message:   com.ilisium.fireapp.model.realms.Message): String? {
        if (message.type ==   com.ilisium.fireapp.model.constants.MessageType.RECEIVED_TEXT &&
            message.content.isNullOrEmpty()
        ) {
            val dataSnapshot =
                  com.ilisium.fireapp.utils.FireConstants.userMessages.child(FireManager.uid).child(message.messageId)
                    .child(  com.ilisium.fireapp.model.constants.DBConstants.CONTENT).toDeffered().await()
            return dataSnapshot.value as? String
        }

        return message.content

    }


}