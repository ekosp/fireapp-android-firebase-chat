package com.ilisium.fireapp.utils.network

import com.ilisium.fireapp.model.realms.User
import com.ilisium.fireapp.utils.RealmHelper
import io.reactivex.Observable

object UserByIdsDataSource {
    fun getUsersByIds(uids: List<String>): Observable<MutableList<  com.ilisium.fireapp.model.realms.User>> {
        val observersList = arrayListOf<Observable<  com.ilisium.fireapp.model.realms.User?>>()

        for (uid in uids) {
            if (uid != FireManager.uid) {
                val user =   com.ilisium.fireapp.utils.RealmHelper.getInstance().getUser(uid)
                if (user != null) {
                    observersList.add(Observable.just(user))
                } else {
                    observersList.add(FireManager.fetchUserByUid(uid).toObservable())
                }

            }
        }


        return Observable.merge(observersList).toList().toObservable().map {
            it.filterNotNull().toMutableList()
        }

    }
}