package com.ilisium.fireapp.utils

import android.Manifest
import android.util.Log
import com.ilisium.fireapp.R
import com.ilisium.fireapp.exceptions.BackupFileMismatchedException
import com.ilisium.fireapp.model.realms.*
import com.ilisium.fireapp.utils.MyApp.Companion.context
import com.ilisium.fireapp.utils.network.FireManager.Companion.uid
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.exceptions.RealmMigrationNeededException
import io.realm.internal.IOException
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.util.*

class RealmBackupRestore {
    private val realm: Realm = Realm.getDefaultInstance()
    private val appFolderName = context().getString(R.string.app_folder_name)

    @Throws(IOException::class)
    fun backup(): File {
        // First check if we have storage permissions


        // create a backup file
        val exportRealmFile = File(EXPORT_REALM_PATH, EXPORT_REALM_FILE_NAME)

        // if backup file already exists, delete it
        exportRealmFile.delete()

        // copy current realm to backup file
        realm.writeCopyTo(exportRealmFile)
        realm.close()
          com.ilisium.fireapp.utils.SharedPreferencesManager.setLastBackup(Date().time)

        return exportRealmFile
    }

    @Throws(
        java.io.IOException::class,
          com.ilisium.fireapp.exceptions.BackupFileMismatchedException::class,
        RealmMigrationNeededException::class
    )
    fun restore(updateLocalPaths: Boolean) {

        //Restore
        val restoreFilePath = "$EXPORT_REALM_PATH/$EXPORT_REALM_FILE_NAME"
        copyBundledRealmFile(restoreFilePath, IMPORT_REALM_FILE_NAME)
        //backed up realm
        val configuration = RealmConfiguration.Builder()
            .name(IMPORT_REALM_FILE_NAME)
            .schemaVersion(  com.ilisium.fireapp.utils.MyMigration.SCHEMA_VERSION.toLong())
            .migration(  com.ilisium.fireapp.utils.MyMigration())
            .build()
        val realm = Realm.getInstance(configuration)
        val currentUserInfo = realm.where(  com.ilisium.fireapp.model.realms.CurrentUserInfo::class.java).findFirst()

        if (currentUserInfo != null && currentUserInfo.uid == uid) {

            val messages = realm.where(  com.ilisium.fireapp.model.realms.Message::class.java).findAll()
            val chats = realm.where(  com.ilisium.fireapp.model.realms.Chat::class.java).findAll()
            val groups = realm.where(  com.ilisium.fireapp.model.realms.Group::class.java).findAll()
            val calls = realm.where(  com.ilisium.fireapp.model.realms.FireCall::class.java).findAll()
            val broadcasts = realm.where(  com.ilisium.fireapp.model.realms.Broadcast::class.java).findAll()

            val instance =   com.ilisium.fireapp.utils.RealmHelper.getInstance()

            for (m in messages) {
                val message = m.clonedMessage
                if (updateLocalPaths) {
                    val localPath = message.localPath

                    if (!localPath.isNullOrEmpty()) {
                        val substring = localPath.substringAfter("/$appFolderName/")
                        val newPath = "${  com.ilisium.fireapp.utils.DirManager.mainAppFolder()}/$substring"
                        message.localPath = newPath
                    }
                }
                instance.saveObjectToRealm(message)
            }
            for (group in groups) {
                instance.saveObjectToRealm(group)
            }
            for (chat in chats) {
                instance.migrateChat(chat)
            }
            for (call in calls) {
                instance.saveObjectToRealm(call)
            }
            for (broadcast in broadcasts) {
                instance.saveObjectToRealm(broadcast)
            }
        } else {
            throw   com.ilisium.fireapp.exceptions.BackupFileMismatchedException()
        }
        realm.close()
        Realm.deleteRealm(configuration)
    }

    @Throws(java.io.IOException::class)
    private fun copyBundledRealmFile(oldFilePath: String, outFileName: String): String {
        val file = File(context().filesDir, outFileName)
          com.ilisium.fireapp.utils.FileUtils.deleteFile(file.path)
        val outputStream = FileOutputStream(file)
        val inputStream = FileInputStream(File(oldFilePath))
        val buf = ByteArray(1024)
        var bytesRead: Int
        while (inputStream.read(buf).also { bytesRead = it } > 0) {
            outputStream.write(buf, 0, bytesRead)
        }
        outputStream.close()
        return file.absolutePath
    }

    private fun dbPath(): String {
        return realm.path
    }

    companion object {
        const val BACKUP_FILE_EXTENSION = "fbup"
        private val EXPORT_REALM_PATH =   com.ilisium.fireapp.utils.DirManager.getDatabasesFolder()
        const val EXPORT_REALM_FILE_NAME = "messages.$BACKUP_FILE_EXTENSION"
        private const val IMPORT_REALM_FILE_NAME = "temp.realm"
        private val TAG = RealmBackupRestore::class.java.name

        // Storage Permissions
        private const val REQUEST_EXTERNAL_STORAGE = 1
        private val PERMISSIONS_STORAGE = arrayOf(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        val isBackupFileExists: Boolean
            get() {
                val exportRealmFile = File(EXPORT_REALM_PATH, EXPORT_REALM_FILE_NAME)
                return exportRealmFile.exists() && exportRealmFile.length() > 0
            }
    }

}
