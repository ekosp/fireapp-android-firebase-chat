package com.ilisium.fireapp.utils.mediastore

import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import androidx.annotation.RequiresApi
import com.ilisium.fireapp.model.constants.MessageType
import com.ilisium.fireapp.model.realms.Message
import java.io.File

object MessageMediaStore {
    @RequiresApi(Build.VERSION_CODES.Q)
    fun saveToMediaStore(message:   com.ilisium.fireapp.model.realms.Message, fileName: String): Uri? {
        return when (message.type) {

              com.ilisium.fireapp.model.constants.MessageType.RECEIVED_IMAGE -> {
                MediaStoreSaver.saveImage(fileName, File(message.localPath))
            }

              com.ilisium.fireapp.model.constants.MessageType.RECEIVED_VIDEO -> {
                MediaStoreSaver.saveVideo(fileName, File(message.localPath))
            }

              com.ilisium.fireapp.model.constants.MessageType.RECEIVED_AUDIO -> {
                MediaStoreSaver.saveAudio(fileName, File(message.localPath))
            }

              com.ilisium.fireapp.model.constants.MessageType.RECEIVED_FILE -> {
                MediaStoreSaver.saveFile(fileName, File(message.localPath))
            }
            else -> null
        }
    }
}