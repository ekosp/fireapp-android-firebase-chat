package com.ilisium.fireapp.utils

import com.ilisium.fireapp.model.constants.DBConstants
import com.ilisium.fireapp.model.constants.DownloadUploadStat
import com.ilisium.fireapp.model.constants.EncryptionType
import com.ilisium.fireapp.model.constants.MessageType
import com.ilisium.fireapp.model.realms.*
import com.ilisium.fireapp.utils.network.FireManager
import com.google.firebase.database.DataSnapshot

object MessageMapper {
    @JvmStatic
    fun mapToMessage(dataSnapshot: DataSnapshot):   com.ilisium.fireapp.model.realms.Message {
        val messageId = dataSnapshot.child(  com.ilisium.fireapp.model.constants.DBConstants.MESSAGE_ID).value as? String ?: ""

        val isGroup = dataSnapshot.hasChild("isGroup")
        //getting data from fcm message and convert it to a message
        val phone = dataSnapshot.child(  com.ilisium.fireapp.model.constants.DBConstants.PHONE).value as? String ?: ""
        val content = dataSnapshot.child(  com.ilisium.fireapp.model.constants.DBConstants.CONTENT).value as? String ?: ""
        val timestamp = dataSnapshot.child(  com.ilisium.fireapp.model.constants.DBConstants.TIMESTAMP).value as? String ?: "0"
        val type = (dataSnapshot.child(  com.ilisium.fireapp.model.constants.DBConstants.TYPE).value as? String ?: "0").toInt()
        //get sender uid
        val fromId = dataSnapshot.child(  com.ilisium.fireapp.model.constants.DBConstants.FROM_ID).value as? String ?: ""
        val toId = dataSnapshot.child(  com.ilisium.fireapp.model.constants.DBConstants.TOID).value as? String ?: ""
        val metadata = dataSnapshot.child(  com.ilisium.fireapp.model.constants.DBConstants.METADATA).value as? String ?: ""

        val encryptionType = dataSnapshot.child(  com.ilisium.fireapp.model.constants.DBConstants.ENCRYPTION_TYPE).value as? String
            ?:   com.ilisium.fireapp.model.constants.EncryptionType.NONE
        //convert sent type to received
        val convertedType =   com.ilisium.fireapp.model.constants.MessageType.convertSentToReceived(type)

        //create the message
        val message =   com.ilisium.fireapp.model.realms.Message()
        message.content = content
        message.timestamp = timestamp
        message.fromId = fromId
        message.type = convertedType
        message.messageId = messageId
        message.metadata = metadata
        message.toId = toId
        val chatId = if (isGroup) toId else fromId
        message.chatId = chatId
        message.isGroup = isGroup
        if (isGroup) message.fromPhone = phone
        //set default state
        message.downloadUploadStat =   com.ilisium.fireapp.model.constants.DownloadUploadStat.FAILED
        message.encryptionType = encryptionType

        //check if it's text message
        if (  com.ilisium.fireapp.model.constants.MessageType.isSentText(type)) {
            //set the state to default
            message.downloadUploadStat =   com.ilisium.fireapp.model.constants.DownloadUploadStat.DEFAULT


            //check if it's a contact
        } else if (dataSnapshot.hasChild(  com.ilisium.fireapp.model.constants.DBConstants.CONTACT)) {
            message.downloadUploadStat =   com.ilisium.fireapp.model.constants.DownloadUploadStat.DEFAULT
            //get the json contact as String
            val contactStr = dataSnapshot.child(  com.ilisium.fireapp.model.constants.DBConstants.CONTACT).value as? String ?: ""
            val realmContact =   com.ilisium.fireapp.model.realms.RealmContact(
                content,
                arrayListOf(),
                contactStr
            )
            message.contact = realmContact


            //check if it's a location message
        } else if (dataSnapshot.hasChild(  com.ilisium.fireapp.model.constants.DBConstants.LOCATION)) {
            message.downloadUploadStat =   com.ilisium.fireapp.model.constants.DownloadUploadStat.DEFAULT

            val lat = dataSnapshot.child("lat").value as? String ?: ""
            val lng = dataSnapshot.child("lng").value as? String ?: ""
            if (lat.isNotEmpty() && lng.isNotEmpty()) {
                val name = dataSnapshot.child("name").value as? String ?: ""
                val address = dataSnapshot.child("address").value as? String ?: ""
                val location =   com.ilisium.fireapp.model.realms.RealmLocation(
                    address,
                    name,
                    lat,
                    lng
                )
                message.location = location
            }

        } else if (dataSnapshot.hasChild(  com.ilisium.fireapp.model.constants.DBConstants.THUMB)) {
            val thumb = dataSnapshot.child(  com.ilisium.fireapp.model.constants.DBConstants.THUMB).value as? String ?: ""

            //Check if it's Video and set Video Duration
            if (dataSnapshot.hasChild(  com.ilisium.fireapp.model.constants.DBConstants.MEDIADURATION)) {
                val mediaDuration = dataSnapshot.child(  com.ilisium.fireapp.model.constants.DBConstants.MEDIADURATION).value as? String
                    ?: ""
                message.mediaDuration = mediaDuration
            }
            message.thumb = thumb


            //check if it's Voice Message or Audio File
        } else if (dataSnapshot.hasChild(  com.ilisium.fireapp.model.constants.DBConstants.MEDIADURATION)
            && type ==   com.ilisium.fireapp.model.constants.MessageType.SENT_VOICE_MESSAGE || type ==   com.ilisium.fireapp.model.constants.MessageType.SENT_AUDIO
        ) {

            //set audio duration
            val mediaDuration = dataSnapshot.child(  com.ilisium.fireapp.model.constants.DBConstants.MEDIADURATION).value as? String ?: ""
            message.mediaDuration = mediaDuration

            //check if it's a File
        } else if (dataSnapshot.hasChild(  com.ilisium.fireapp.model.constants.DBConstants.FILESIZE)) {
            val fileSize = dataSnapshot.child(  com.ilisium.fireapp.model.constants.DBConstants.FILESIZE).value as? String ?: ""
            message.fileSize = fileSize
        }

        //if the message was quoted save it and get the quoted message
        if (dataSnapshot.hasChild("quotedMessageId")) {
            val quotedMessageId = dataSnapshot.child("quotedMessageId").value as? String ?: ""
            //sometimes the message is not saved because of threads,
            //so we need to make sure that we refresh the database before checking if the message is exists
              com.ilisium.fireapp.utils.RealmHelper.getInstance().refresh()
            val quotedMessage =   com.ilisium.fireapp.utils.RealmHelper.getInstance().getMessage(quotedMessageId, chatId)
            if (quotedMessage != null)
                message.quotedMessage =   com.ilisium.fireapp.model.realms.QuotedMessage.messageToQuotedMessage(quotedMessage)
        }

        //if the message was quoted save it and get the quoted message
        if (dataSnapshot.hasChild("statusId")) {
            val statusId = dataSnapshot.child("statusId").value as? String ?: ""
            //sometimes the message is not saved because of threads,
            //so we need to make sure that we refresh the database before checking if the message is exists
              com.ilisium.fireapp.utils.RealmHelper.getInstance().refresh()
            val status =   com.ilisium.fireapp.utils.RealmHelper.getInstance().getStatus(statusId)
            if (status != null) {
                message.status = status
                val quotedMessage =   com.ilisium.fireapp.model.realms.Status.statusToMessage(status, fromId)
                quotedMessage?.fromId = FireManager.uid
                quotedMessage?.chatId = fromId
                if (quotedMessage != null)
                    message.quotedMessage =   com.ilisium.fireapp.model.realms.QuotedMessage.messageToQuotedMessage(quotedMessage)
            }

        }

        return message
    }
}