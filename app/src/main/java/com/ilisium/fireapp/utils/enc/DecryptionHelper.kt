package com.ilisium.fireapp.utils.enc

import com.ilisium.fireapp.model.constants.EncryptionType
import com.ilisium.fireapp.utils.enc.aes.AESCrypto
import com.ilisium.fireapp.utils.enc.ethree.EthreeHelper
import kotlinx.coroutines.CoroutineScope

class DecryptionHelper {

    private val aesCrypto:   com.ilisium.fireapp.utils.enc.aes.AESCrypto by lazy {
          com.ilisium.fireapp.utils.enc.aes.AESCrypto()
    }


    suspend fun decrypt(
        scope: CoroutineScope,
        fromId: String,
        message: String,
        encryptionType: String
    ): String {
        return when {
            encryptionType.equals(
                  com.ilisium.fireapp.model.constants.EncryptionType.AES,
                ignoreCase = true
            ) -> aesCrypto.decryptCipherTextWithRandomIV(message)
            encryptionType.equals(
                  com.ilisium.fireapp.model.constants.EncryptionType.E2E,
                ignoreCase = true
            ) -> EthreeHelper.decryptMessage(scope, fromId, message)
            else -> message
        }
    }


}