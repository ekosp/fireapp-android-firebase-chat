/*
 * Created by Devlomi on 2020
 */

package com.ilisium.fireapp.utils

import android.util.Log
import com.ilisium.fireapp.activities.calling.model.CallType
import com.ilisium.fireapp.model.constants.DBConstants
import com.ilisium.fireapp.model.constants.FireCallDirection
import com.ilisium.fireapp.model.realms.FireCall
import com.ilisium.fireapp.model.realms.Group
import com.ilisium.fireapp.model.realms.User
import com.ilisium.fireapp.utils.network.FireManager
import com.google.firebase.database.DataSnapshot

object CallMapper {
    fun mapToFireCall(dataSnapshot: DataSnapshot):   com.ilisium.fireapp.model.realms.FireCall? {

        if (dataSnapshot.value != null) {
            (dataSnapshot.child(  com.ilisium.fireapp.model.constants.DBConstants.CALL_ID).value as? String)?.let { callId ->


                val fromId = dataSnapshot.child("callerId").value as? String ?: ""

                val typeInt = (dataSnapshot.child("callType").value as? Long)?.toInt() ?: CallType.VOICE.value
                val type = CallType.fromInt(typeInt)


                val groupId = dataSnapshot.child("groupId").value as? String ?: ""

                val isGroupCall = type.isGroupCall()

                if (!isGroupCall && FireManager.uid.isEmpty()) return@let
                if (isGroupCall && groupId.isEmpty()) return@let
                val channel = dataSnapshot.child("channel").value as? String ?: return@let

                val groupName = dataSnapshot.child("groupName").value as? String ?: ""

                val timestamp = dataSnapshot.child("timestamp").value as? Long
                        ?: System.currentTimeMillis()
                val phoneNumber = dataSnapshot.child("phoneNumber").value as? String ?: ""

                val isVideo = type.isVideo()

                val uid = if (isGroupCall) groupId else fromId


                var user:   com.ilisium.fireapp.model.realms.User

                val storedUser =   com.ilisium.fireapp.utils.RealmHelper.getInstance().getUser(uid)

                if (storedUser == null) {
                    //make dummy user temporarily
                    user =   com.ilisium.fireapp.model.realms.User().apply {
                        if (isGroupCall) {
                            this.uid = groupId!!
                            this.isGroupBool = true
                            this.userName = groupName
                            this.group =   com.ilisium.fireapp.model.realms.Group()
                                .apply {
                                this.groupId = groupId
                                this.isActive = true
                                this.setUsers(mutableListOf(  com.ilisium.fireapp.utils.SharedPreferencesManager.getCurrentUser()))
                            }

                        } else {
                            this.uid = uid
                            this.phone = phoneNumber
                        }
                    }
                } else {
                    user = storedUser
                }

                return   com.ilisium.fireapp.model.realms.FireCall(
                    callId,
                    user,
                      com.ilisium.fireapp.model.constants.FireCallDirection.INCOMING,
                    timestamp,
                    phoneNumber,
                    isVideo,
                    typeInt,
                    channel
                )


            }

        }
        return null

    }
}